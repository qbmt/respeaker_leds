# Respeaker Leds

> This is an application to change the colors of the leds from Respeaker Core v2.

## Installation

```bash
sudo apt install python-mraa libmraa1
pip install pixel-ring
pip install paho-mqtt
pip install -U python-dotenv
```

## Dotenv

The repository uses an .env file. In the file the ip address of the server is stored along withe the mac address of the ReSpeaker Core v2.0. To get the ip address of the MQTT broker you have to run another [repository](https://bitbucket.org/qbmt/discovery_tool_respeaker/src/master/). After you've run the repository you can run the following command to put the mac address in the .env file.

You have to run the script once to make the environment variables.

```bash
make dotenv
```

## Run

```bash
make run
```

## Meta

Arthur Verstraete - [ArthurVerstraete](https://github.com/ArthurVerstraete) - arthur.verstraete@student.vives.be

Frederik Feys – [FrederikFeys](https://github.com/FrederikFeys) – frederik.feys@student.vives.be
