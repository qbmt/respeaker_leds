import time

from pixel_ring import pixel_ring
import mraa
import os
import paho.mqtt.client as mqtt

from dotenv import load_dotenv
load_dotenv()

mqttc = mqtt.Client(os.getenv("MAC_ADDRESS") + "-leds", clean_session=False)

en = mraa.Gpio(12)
if os.geteuid() != 0 :
    time.sleep(1)

en.dir(mraa.DIR_OUT)
en.write(0)


def ready():
    pixel_ring.set_brightness(5)
    for x in range(0, 5, 1):
        pixel_ring.wakeup()
    off()

def off():
    pixel_ring.off()

def triggered():
    pixel_ring.set_brightness(10)
    pixel_ring.set_color(r=0, g=0, b=255)

def listen(mic):
    mic = int(mic)
    if (mic != 2):
        if (mic != 1):
            before = (2*mic)-5
            after = 9-before
        else:
            before = 9
            after = 0
        pixels = [0, 0, 0, 255] * before + [0, 255, 0, 0] * 3 + [0, 0, 0, 255] * after
    else:
        pixels = [0, 255, 0, 0] * 2 + [0, 0, 0, 255] * 9 + [0, 255, 0, 0]

    pixel_ring.set_brightness(10)
    pixel_ring.show(pixels)


def think():
    pixel_ring.set_brightness(10)
    for x in range(35, 226, 5):
        pixel_ring.set_color(r=x, g=35, b=0)
        time.sleep(0.02)

    for x in reversed(range(35, 226, 5)):
        pixel_ring.set_color(r=x, g=35, b=0)
        time.sleep(0.02)
    off()

def understood():
    pixel_ring.set_brightness(20)
    for x in range(5, 226, 5):
        pixel_ring.set_color(r=0, g=x, b=0)
        time.sleep(0.02)

    for x in reversed(range(5, 226, 5)):
        pixel_ring.set_color(r=0, g=x, b=0)
        time.sleep(0.02)
    off()

def mis_understood():
    pixel_ring.set_brightness(20)
    for x in range(5, 226, 5):
        pixel_ring.set_color(r=x, g=0, b=0)
        time.sleep(0.02)

    for x in reversed(range(5, 226, 5)):
        pixel_ring.set_color(r=x, g=0, b=0)
        time.sleep(0.02)
    off()

def on_connect(mqttc, obj, flags, rc):
    print("rc: " + str(rc))
    mqttc.subscribe("leds/" + os.getenv("MAC_ADDRESS"), 0)


def on_message(mqttc, obj, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
    if str(msg.payload) == "ready":
        ready()
    if str(msg.payload[0:6]) == "listen":
        mic = msg.payload[6:7]
        listen(mic)
    if str(msg.payload) == "understood":
        understood()
    if str(msg.payload) == "mis_understood":
        mis_understood()
    if str(msg.payload) == "off":
        off()
    if str(msg.payload) == "think":
        think()
    if str(msg.payload) == "KWS triggered":
        triggered()


def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))

# def on_unsubscribe():
#     subscribe()

# def subscribe():
#     mqttc.subscribe("leds/" + os.getenv("MAC_ADDRESS"), 0)
#     mqttc.loop_forever()

def on_log(mqttc, obj, level, string):
    print(string)

def connect():
    try:
        mqttc.connect(os.getenv("SERVER_ADDRESS"), 1883, 60)
        # subscribe()
    except:
        time.sleep(5)
        connect()

def on_disconnect():
    connect()

# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe
# mqttc.on_unsubscribe = on_unsubscribe
mqttc.on_disconnect = on_disconnect
# Uncomment to enable debug messages
# mqttc.on_log = on_log
connect()
# mqttc.subscribe("leds/" + os.getenv("MAC_ADDRESS"), 0)
mqttc.loop_forever()

en.write(1)
